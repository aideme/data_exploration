/*
 * Copyright (c) 2019 École Polytechnique
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, you can obtain one at http://mozilla.org/MPL/2.0
 *
 * Authors:
 *       Luciano Di Palma <luciano.di-palma@polytechnique.edu>
 *       Enhui Huang <enhui.huang@polytechnique.edu>
 *       Laurent Cetinsoy <laurent.cetinsoy@gmail.com>
 *
 * Description:
 * AIDEme is a large-scale interactive data exploration system that is cast in a principled active learning (AL) framework: in this context,
 * we consider the data content as a large set of records in a data source, and the user is interested in some of them but not all.
 * In the data exploration process, the system allows the user to label a record as “interesting” or “not interesting” in each iteration,
 * so that it can construct an increasingly-more-accurate model of the user interest. Active learning techniques are employed to select
 * a new record from the unlabeled data source in each iteration for the user to label next in order to improve the model accuracy.
 * Upon convergence, the model is run through the entire data source to retrieve all relevant records.
 */

package utils.linprog;

import exceptions.NoFeasibleSolutionException;
import exceptions.UnboundedSolutionException;
import gurobi.*;
import utils.Validator;

public class GurobiSolver implements LinearProgramSolver {

    private final int dim;
    private GRBVar[] variables;
    private GRBModel model;

    public GurobiSolver(int dim) {
        this.dim = dim;

        try {
            GRBEnv env = new GRBEnv();

            env.set(GRB.IntParam.OutputFlag, 0);  // disable console logging
            env.set(GRB.DoubleParam.FeasibilityTol, 1e-9);  // set constraint violation tolerance to minimum allowed value

            model = new GRBModel(env);

            // Create variables
            variables = new GRBVar[dim];
            for (int i = 0; i < dim; i++) {
                variables[i] = model.addVar(-GRB.INFINITY, GRB.INFINITY, 0, GRB.CONTINUOUS, "x" + i);
            }
        } catch (GRBException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setObjectiveFunction(double[] vector) {
        Validator.assertEquals(vector.length, dim);

        GRBLinExpr expr = new GRBLinExpr();
        for (int i = 0; i < dim; i++) {
            expr.addTerm(vector[i], variables[i]);
        }

        try {
            model.setObjective(expr, GRB.MINIMIZE);
        } catch (GRBException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setLower(double[] vector) {
        Validator.assertEquals(vector.length, dim);
        try {
            for (int i = 0; i < dim; i++) {
                variables[i].set(GRB.DoubleAttr.LB, vector[i]);
            }
        } catch (GRBException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setUpper(double[] vector) {
        Validator.assertEquals(vector.length, dim);
        try {
            for (int i = 0; i < dim; i++) {
                variables[i].set(GRB.DoubleAttr.UB, vector[i]);
            }
        } catch (GRBException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public double[] findMinimizer() {
        try {
            model.optimize();

            int status = model.get(GRB.IntAttr.Status);

            if (status == GRB.Status.INFEASIBLE) {
                throw new NoFeasibleSolutionException();
            }

            if (status == GRB.Status.UNBOUNDED) {
                throw new UnboundedSolutionException();
            }

            return model.get(GRB.DoubleAttr.X, model.getVars());

        } catch (GRBException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void addLinearConstrain(double[] vector, InequalitySign inequalitySign, double val) {
        Validator.assertEquals(vector.length, dim);

        GRBLinExpr expr = new GRBLinExpr();
        for (int i = 0; i < dim; i++) {
            expr.addTerm(vector[i], variables[i]);
        }

        try {
            model.addConstr(
                    expr,
                    inequalitySign == InequalitySign.LEQ ? GRB.LESS_EQUAL : GRB.GREATER_EQUAL,
                    val,
                    "c" + model.getConstrs().length
            );
        } catch (GRBException ex) {
            throw new RuntimeException(ex);
        }
    }
}
